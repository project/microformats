<?php

namespace Drupal\microformats\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\microformats\Utility;

/**
 * Provides a 'Contact Info' block.
 *
 * @Block(
 *   id = "microformats_contactinfo_block",
 *   admin_label = @Translation("Microformats Contact Info Block")
 * )
 */
class ContactInfoBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
//    $config = $this->getConfiguration();
//    var_dump($config);
    //$render = array(
      //'#markup' => $this->t('Hello, World!'),
      //'#theme' => 'microformats_contactinfo_block',
    //);
    $variables = Utility::getMicroformatsContactInfoVars();
    $id = '';
    if (isset($variables['#id']) && $variables['#id']) {
      $id = '__' . str_replace('-', '_', $variables['#id']);
    }
    $render = ['#theme' => 'microformats_sitewide_contactinfo' . $id];
    $render = array_merge($render, $variables);
    $render['#attached']['library'][] = 'microformats/microformats';
    return $render;
  }

}
